# 给你一个整型数组 nums ，在数组中找出由三个数组成的最大乘积，并输出这个乘积。
# 关键：最大乘积的意思为 三个数乘积所得的实数最大
# 解题思路：
# 三个数的正负可能性：--- 、+++ 、--+
# 直接排序取得，负数最大值和正数最大值
class Solution:
    def maximumProduct(self, nums: list[int]) -> int:
        nums.sort()
        return max(nums[0]*nums[1]*nums[-1],nums[-1]*nums[-2]*nums[-3])

s=Solution()
nums=list(map(int,input().strip().split()))
print(s.maximumProduct(nums))