# 句子 是一串由空格分隔的单词。每个 单词 仅由小写字母组成。
#
# 如果某个单词在其中一个句子中恰好出现一次，在另一个句子中却 没有出现 ，那么这个单词就是 不常见的 。
#
# 给你两个 句子 s1 和 s2 ，返回所有 不常用单词 的列表。返回列表中单词可以按 任意顺序 组织。

class Solution:
    def uncommonFromSentences(self, s1: str, s2: str) -> list[str]:
        arr=[]
        for i in s1+s2:
            # 在for循环的基础上，利用count函数进行计数
            if (s1+s2).count(i)==1:
                arr.append(i)
        return arr

s=Solution()
ch=[]
for i in range (2):
    ch.append(input().strip().split())
print(ch[0])
print(ch[1])
print(s.uncommonFromSentences(ch[0],ch[1]))