n,m=map(int,input().strip().split())
map_1,map_3=[],[]
for i in range(n):
    map_1.append(list(input()))
for i in range(n):
    map_2=[]
    for j in range(m):
        if map_1[i][j]=='*':
            map_2.append('*')
        else:
            # count 雷的计数器
            count=0
            # 利用这两个循环遍历周围雷的数量
            for k in range(i-1,i+2):
                for l in range(j-1,j+2):
                    # 只执行在边界类的操作
                    if k in range(n) and l in range(m):
                        if map_1[k][l]=='*':
                            count+=1
            map_2.append(count)
    map_3.append(map_2)
for i in range(n):
    for j in range(m):
        print(map_3[i][j],end='')
    print()