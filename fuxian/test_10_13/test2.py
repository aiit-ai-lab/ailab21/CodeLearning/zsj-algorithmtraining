while True:
    try:
        # 字符串按边长字母长度分割
        def cut(obj, sec):
            arr=[]
            for i in range(0, len(obj), sec):
                arr.append(obj[i:i + sec])
            return arr

        n = int(input())
        a = 2 * n + 1
        v = 'abcdefghijklmnopqrstuvwxyz'
        # 计算a~z次数
        all_v_times = a * a // 26 + 1
        # all_v 字母总数
        all_v = v * all_v_times
        ch=cut(all_v,a)
        for i in range(a):
            # 已经到了最后一个，控制循环结束
            if i == a :
                break
            # 偶数项，则倒序
            if i%2==1:
                tep=ch[i]
                ch[i]=tep[::-1]
            print(ch[i])

    except EOFError:
        break