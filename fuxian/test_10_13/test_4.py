def multi_s(s1,s2):
    # 字符串翻转
    s1=list(s1[::-1])
    s2=list(s2[::-1])
    s3=[]
    for i in range(len(s2)):
        # 计算余数和进位
        yu,jin=0,0
        yu=(int(s1[i])+int(s2[i]))%10
        jin=(int(s1[i])+int(s2[i]))//10
        if jin!=0:
            # 将进位直接加到较长数的下一位数
            s1[i+1]=int(s1[i+1])+jin
        s3.append(yu)
    # 将长度超出的部分进行存放
    if len(s3)<len(s1):
        for j in range(len(s3),len(s1)):
            s3.append(int(s1[j]))
    # 用字符串的形式倒序输出
    s3=s3[::-1]
    print(''.join(map(str,s3)))

s1,s2=map(str,input().strip().split())
# 判断长短
if len(s1) < len(s2):
    temp = s1
    s1 = s2
    s2 = temp
multi_s(s1, s2)