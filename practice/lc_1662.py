# 给你两个字符串数组 word1 和 word2 。如果两个数组表示的字符串相同，返回 true ；否则，返回 false 。
# 数组表示的字符串是由数组中的所有元素 按顺序 连接形成的字符串。
class Solution:
    def arrayStringsAreEqual(self, word1: list[str], word2: list[str]) -> bool:
        return ''.join(word1) == ''.join(word2)

s=Solution()
word1=list(map(str,input().strip().split()))
word2=list(map(str,input().strip().split()))
print(s.arrayStringsAreEqual(word1,word2))