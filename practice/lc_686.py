# 给定两个字符串a 和 b，寻找重复叠加字符串 a 的最小次数，使得字符串 b 成为叠加后的字符串 a 的子串，
# 如果不存在则返回 -1。
# 注意：字符串 "abc"重复叠加 0 次是 ""，重复叠加 1 次是"abc"，重复叠加 2 次是"abcabc"。

import math

class Solution:
    def repeatedStringMatch(self, a: str, b: str) -> int:
        # 利用集合思想
        sa,sb=set(a),set(b)
        if a is b:
            return 1
        # 如果交集不为字符b，则b不可能在a里面
        if sa & sb != sb:
            return -1
        # 利用ab关系，算出a的长度
        # math.ceil（）向上取整
        times= math.ceil(len(b)/len(a))
        if (b in a*times):
            return times
        elif (b in a*(times+1)):
            return times+1
        else:
            return -1
s=Solution()
a,b=map(str,input().strip().split())
print(s.repeatedStringMatch(a,b))