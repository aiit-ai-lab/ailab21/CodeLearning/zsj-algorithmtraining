# 给你长度相等的两个字符串 s1 和 s2 。一次 字符串交换 操作的步骤如下：选出某个字符串中的两个下标（不必不同），
# 并交换这两个下标所对应的字符。
# 如果对 其中一个字符串 执行 最多一次字符串交换 就可以使两个字符串相等，返回 true ；否则，返回 false 。
class Solution:
    def areAlmostEqual(self, s1: str, s2: str) -> bool:
        a,b,count=0,0,0
        if len(s1)!=len(s2):
            return False
        for i in range(len(s1)):
            # 如果遍历到他们字母不相同的地方，就用a，b，将位置存下来
            # 由于题目只变换两个数，所以用count记录不同位置的次数，且用a，b就足够了
            if s1[i]!=s2[i]:
                if count==0:
                    a=i
                if count==1:
                    b=i
                count+=1
        if count != 2:
            return False
        else:
            if s1[a]==s2[b] and s1[b]==s2[a]:
                return True
            else:
                return False
s=Solution()
a,b=map(str,input().strip().split())
print(s.areAlmostEqual(a,b))