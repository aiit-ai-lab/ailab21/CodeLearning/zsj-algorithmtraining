# 给你两个数 hour 和 minutes 。
# 请你返回在时钟上，由给定时间的时针和分针组成的较小角的角度（60 单位制）。
class Solution:
    def angleClock(self, hour: int, minutes: int) -> float:
        h = (hour*30+minutes*0.5) % 360
        m = minutes*6
        return min(abs(h-m), 360-abs(h-m))
s=Solution()
a,b=map(int,input().strip().split())
print(s.angleClock(a,b))